# !/usr/bin/python
# -*- coding: utf8 -*-

import unittest
import editor

instancia = editor.Matriz()

class MatrizTestes(unittest.TestCase):
    def teste_insercao_sucesso(self):
        linha = ["O"]*2
        matriz = [[x for x in linha] for _ in range(0,2)]
        self.assertEqual(matriz, instancia.insercao(2,2)) 
    def teste_insercao_fracasso(self):
        self.assertRaises(TypeError, instancia.insercao(2,2)) 
    def teste_insercao_nula(self):
        with self.assertRaises(TypeError):
         try:
             instancia.insercao(None, None)
         except TypeError as e:
             self.assertEqual(e.args,
                              ('Não é possível ter colunas ou linhas nulas',))
             raise            
    def teste_limpar_sucesso(self):
        limpar = instancia.limpar()
        self.assertEqual(True, limpar) 
    def teste_limpar_fracasso(self):
        fracasso = editor.Matriz()
        with self.assertRaises(TypeError):
         try:
             fracasso.limpar()
         except TypeError as e:
             self.assertEqual(e.args,
                              ('Não é possível limpar a matriz nula',))
             raise        
    def teste_colorir_pixel_sucesso(self):
        instancia.insercao(3,2)
        colorir = instancia.colorir_pixel(1,1,"r")
        self.assertEqual(True, colorir) 
    def teste_colorir_fracasso_x(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_pixel(10,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento 10 é inválido',))
             raise           
    def teste_colorir_fracasso_y(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_pixel(1,10,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento 10 é inválido',))
             raise           
    def teste_colorir_fracasso_negativo_x(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_pixel(-1,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento -1 é inválido',))
             raise           
    def teste_colorir_fracasso_negativo_y(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_pixel(1,-1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento -1 é inválido',))
             raise                     
    def teste_colorir_fracasso_cor(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_pixel(1,1,3)
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor da cor não está de acordo com o estabelecido',))
             raise   
    def teste_colorir_vertical_sucesso(self):
        instancia.insercao(5,6)
        self.assertEqual(True, instancia.colorir_vertical_pixel(3, 2, 4, "A"))
    def teste_colorir_vertical_fracasso_coluna(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_vertical_pixel(10,1,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento 10 é inválido',))
             raise           
    def teste_colorir_vertical_fracasso_linha_inicio(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_vertical_pixel(1,7,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento 7 é inválido',))
             raise           
    def teste_colorir_vertical_fracasso_negativo_linha_inicio(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_vertical_pixel(1,-1,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento -1 é inválido',))
             raise           
    def teste_colorir_vertical_fracasso_negativo_linha_fim(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_vertical_pixel(1, 1,-1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento -1 é inválido',))
             raise      
    def teste_colorir_vertical_fracasso_linha_fim(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_vertical_pixel(1, 1,10,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento 10 é inválido',))
             raise                     
    def teste_colorir_vertical_fracasso_cor(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_vertical_pixel(2,1,1,3)
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor da cor não está de acordo com o estabelecido',))
             raise                     
    def teste_colorir_horizontal_sucesso(self):
        instancia.insercao(5,6)
        self.assertEqual(True, instancia.colorir_horizontal_pixel(3, 2, 4, "A"))
    def teste_colorir_horizontal_fracasso_linha(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_horizontal_pixel(10,1,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento 10 é inválido',))
             raise           
    def teste_colorir_horizontal_fracasso_coluna_inicio(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_horizontal_pixel(1,7,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento 7 é inválido',))
             raise           
    def teste_colorir_horizontal_fracasso_negativo_coluna_inicio(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_horizontal_pixel(1,-1,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento -1 é inválido',))
             raise           
    def teste_colorir_horizontal_fracasso_negativo_coluna_fim(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_horizontal_pixel(1, 1,-1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento -1 é inválido',))
             raise      
    def teste_colorir_horizontal_fracasso_coluna_fim(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_horizontal_pixel(1, 1,10,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento 10 é inválido',))
             raise                     
    def teste_colorir_horizontal_fracasso_cor(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_horizontal_pixel(2,1,1,3)
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor da cor não está de acordo com o estabelecido',))
             raise    
    def teste_colorir_pixel_retangulo_sucesso(self):
        instancia.insercao(4,4)
        colorir = instancia.colorir_retangulo_pixel(1,1,4,4,"r")
        self.assertEqual(True, colorir) 
    def teste_colorir_retangulo_fracasso_x1(self):
        instancia.insercao(4,4)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_retangulo_pixel(10,2,1,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento 10 é inválido',))
             raise           
    def teste_colorir_retangulo_fracasso_y1(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_retangulo_pixel(1,20,1,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento 20 é inválido',))
             raise           
    def teste_colorir_retangulo_fracasso_negativo_x1(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_retangulo_pixel(-1,1,1,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento -1 é inválido',))
             raise           
    def teste_colorir_retangulo_fracasso_negativo_y1(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_retangulo_pixel(1,-1,1,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento -1 é inválido',))
             raise                     
    def teste_colorir_retangulo_fracasso_x2(self):
        instancia.insercao(4,4)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_retangulo_pixel(1,2,10,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento 10 é inválido',))
             raise           
    def teste_colorir_retangulo_fracasso_y2(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_retangulo_pixel(1,2,1,10,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento 10 é inválido',))
             raise           
    def teste_colorir_retangulo_fracasso_negativo_x2(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_retangulo_pixel(1,1,-1,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento -1 é inválido',))
             raise           
    def teste_colorir_retangulo_fracasso_negativo_y2(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_retangulo_pixel(1,1,1,-1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento -1 é inválido',))
             raise
    def teste_colorir_retangulo_fracasso_cor(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_pixel(1,1,3)
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor da cor não está de acordo com o estabelecido',))
             raise     
    def teste_colorir_pixel_regiao_sucesso(self):
        instancia.insercao(3,2)
        colorir = instancia.colorir_regiao_pixel(1,1,"r")
        self.assertEqual(True, colorir) 
    def teste_colorir_regiao_fracasso_x(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_regiao_pixel(10,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento 10 é inválido',))
             raise           
    def teste_colorir_regiao_fracasso_y(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_regiao_pixel(1,10,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento 10 é inválido',))
             raise           
    def teste_colorir_regiao_fracasso_negativo_x(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_regiao_pixel(-1,1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento -1 é inválido',))
             raise           
    def teste_colorir_regiao_fracasso_negativo_y(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_regiao_pixel(1,-1,"r")
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor do argumento -1 é inválido',))
             raise                     
    def teste_colorir_regiao_fracasso_cor(self):
        instancia.insercao(3,2)
        with self.assertRaises(TypeError):
         try:
             instancia.colorir_regiao_pixel(1,1,3)
         except TypeError as e:
             self.assertEqual(e.args,
                              ('O valor da cor não está de acordo com o estabelecido',))
             raise                  
if __name__ == "__main__":
    unittest.main()