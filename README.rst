*************************************
Solução para teste na Evolux
*************************************

Autora
=======

- Irene Ginani Costa Pinheiro

Descrição da prova
==================

Dada uma matriz de tamanho MxN na qual cada elemento represente um pixel, crie
um programa que leia uma sequência de comandos e os interprete manipulando a
matriz de acordo com a descrição abaixo de cada comando.

Comandos:
----------

I M N
______

Cria uma nova matriz MxN. Todos os pixels são brancos (O).

C
__
Limpa a matriz. O tamanho permanece o mesmo. Todos os pixels ficam brancos (O).

L X Y C
_______
Colore um pixel de coordenadas (X,Y) na cor C.

V X Y1 Y2 C
____________

Desenha um segmento vertical na coluna X nas linhas de Y1 a Y2 (intervalo
inclusivo) na cor C.

H X1 X2 Y C
___________

Desenha um segmento horizontal na linha Y nas colunas de X1 a X2 (intervalo
inclusivo) na cor C.

K X1 Y1 X2 Y2 C
________________

Desenha um retangulo de cor C. (X1,Y1) é o canto superior esquerdo e (X2,Y2) o
canto inferior direito.

F X Y C
________
Preenche a região com a cor C. A região R é definida da seguinte forma:
O pixel (X,Y) pertence à região. Outro pixel pertence à região, se e somente se,
ele tiver a mesma cor que o pixel (X,Y) e tiver pelo menos um lado em comum com
um pixel pertencente à região.

S Name
______
Escreve a imagem em um arquivo de nome Name.

X
__
Encerra o programa.

Considerações
______________

Comandos diferentes de I, C, L, V, H, K, F, S e X devem ser ignorados

Execução
=========

- Para que se executem os testes realize clone deste projeto, entre na pasta do projeto e realize o seguinte comando:

	python testes.py

Você logo verificará a saída dos testes

- Para que execute o programa e o utilize como desejar é necessário que você também esteja dentro da pasta do projeto e execute o seguinte comando:

	python editor.py

Então os comandos descritos anteriormente estarão disponíveis. Atente para a entrada do comando, é necessário seguir o padrão dito acima para que você possa executar o que deseja!
