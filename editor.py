# !/usr/bin/python
# -*- coding: utf8 -*-

from os import sys

class Matriz(object):
    def __init__(self):
        self.matriz = None
        self.regiao = None
    def insercao(self,linha,coluna):
        matriz = []
        if (linha is None) or (coluna is None):
            raise TypeError("Não é possível ter colunas ou linhas nulas")  
        elif (linha < 0) or (coluna < 0):
            raise TypeError("Não é possível ter colunas ou linhas negativas")
        else:
            linhas = ["O"]*linha
            self.matriz = [[x for x in linhas] for _ in range(0,coluna)]
            self.regiao = [[x for x in linhas] for _ in range(0,coluna)]
            return self.matriz
    def validar_argumentos(self, argumentos, cor):
        for i in range(0, len(argumentos)):
            if(self.matriz is None):
                raise TypeError("Não é possível ter a matriz nula")
            elif (argumentos[i] > len(self.matriz)) or argumentos[i] < 0:
                raise TypeError("O valor do argumento "+str(argumentos[i])+" é inválido")
            elif (type(cor) is not str):
                raise TypeError("O valor da cor não está de acordo com o estabelecido") 
        return True          
    def transformar_string(self):
        stringMatriz = []
        if (self.matriz is None):
            stringMatriz.append("Matriz vazia")
        else:
            for i in range(0, len(self.matriz)):
                stringMatrizLinha = ""
                for j in range(0, len(self.matriz[0])):
                    stringMatrizLinha = stringMatrizLinha + " " + self.matriz[i][j]
                stringMatriz.append(stringMatrizLinha)
                stringMatriz.append("\n")
        return stringMatriz
    def limpar(self):
        if(self.matriz is None):
            raise TypeError("Não é possível limpar a matriz nula")
        else:
            linhas = ["O"]*len(self.matriz[0])
            self.matriz = [[x for x in linhas] for _ in range(0,len(self.matriz))]
            return True
    def limpar_regiao(self):
        if(self.regiao is None):
            raise TypeError("Não é possível limpar a matriz nula")
        else:
            linhas = ["O"]*len(self.regiao[0])
            self.regiao = [[x for x in linhas] for _ in range(0,len(self.regiao))]
            return True
    def colorir_pixel(self, pixelX, pixelY, cor):
        argumentos = [pixelX, pixelY]
        if(self.validar_argumentos(argumentos, cor)):
            self.matriz[pixelY-1][pixelX-1] = cor
            
            return True
    def colorir_vertical_pixel(self, coluna, linhaInicio, linhaFim, cor):
        argumentos = [coluna, linhaInicio, linhaFim]
        if(self.validar_argumentos(argumentos, cor)):
            if linhaInicio < linhaFim:
                for i in range(linhaInicio-1, linhaFim):
                    self.matriz[i][coluna-1] = cor
            else:
                for i in range(linhaFim-1, linhaInicio):
                    self.matriz[i][coluna-1] = cor
            return True
    def colorir_horizontal_pixel(self, linha, colunaInicio, colunaFim, cor):
        argumentos = [linha, colunaInicio, colunaFim]
        if(self.validar_argumentos(argumentos, cor)):
            if colunaInicio < colunaFim:
                for i in range(colunaInicio-1, colunaFim):
                    self.matriz[linha-1][i] = cor
            else:
                for i in range(colunaFim-1, colunaInicio):
                    self.matriz[linha-1][i] = cor
            return True
    def colorir_retangulo_pixel(self, linha1, coluna1, linha2, coluna2, cor):
        argumentos = [linha1, coluna1, linha2, coluna2]
        if(self.validar_argumentos(argumentos, cor)):
            for i in range(linha1-1,linha2):
                for j in range(coluna1-1, coluna2):
                    self.matriz[i][j] = cor
            return True 
    def definir_borda_matriz(self, pixelX, pixelY):
        if (pixelX > len(self.matriz)) or (pixelY > len(self.matriz[0])) or pixelX < 0 or pixelY < 0:
            return True
        return False
    def definir_alem_borda_matriz(self,pixelX,pixelY):
        if (pixelX+1 >= len(self.matriz)) or (pixelY+1 >= len(self.matriz[0])) or pixelX-1 < 0 or pixelY-1 < 0:
            return True
        return False
    def definir_borda_regiao(self, pixelX, pixelY, i, j):
        if ((pixelX+1 == len(self.matriz)) or (pixelY+1 == len(self.matriz[0]))) and self.matriz[i][j] == self.matriz[pixelX][pixelY]:
            return True
        return False
    def definir_alem_borda_regiao(self, pixelX, pixelY, i, j):
        if ((pixelX-1 == 0) or (pixelY-1 == 0) and self.matriz[i][j] == self.matriz[pixelX][pixelY]):
            return True
        return False
    def definir_regiao(self, pixelX, pixelY):
        if self.definir_borda_matriz(pixelX, pixelY):
            return
        elif self.definir_alem_borda_matriz(pixelX, pixelY):
            return
        else:
            for i in range(pixelX-1, pixelX+2):
                for j in range(pixelY-1, pixelY+2):
                    if (not self.regiao[i][j] == "1"):
                        if self.definir_borda_regiao(pixelX, pixelY, i,j):
                            self.regiao[i][j] = "1"
                        elif self.definir_alem_borda_regiao(pixelX, pixelY, i, j):
                            self.regiao[i][j] = "1"
                            self.definir_regiao(i,j)
                        elif self.matriz[pixelX][pixelY] == self.matriz[i][j]:
                            self.regiao[i][j] = "1"
                            self.definir_regiao(i,j)
    def colorir_regiao_pixel(self, pixelX, pixelY,cor):
        argumentos = [pixelX, pixelY]
        if(self.validar_argumentos(argumentos, cor)):
            self.definir_regiao(pixelX-1,pixelY-1)
            for i in range(0, len(self.matriz)):
                for j in range(0, len(self.matriz[0])):
                    if(self.regiao[i][j] == "1"):
                        self.matriz[i][j] = cor
            self.matriz[pixelX-1][pixelY-1] = cor
            self.limpar_regiao()
            return True
def main():
    matriz = Matriz()
    while (True):
        opcao = input()
        argumentos = opcao.split(" ")
        if (argumentos[0] == "I"):
            if (len(argumentos) == 3):
                try:
                    m = int (argumentos[1])
                    n = int( argumentos[2])
                    matriz.insercao(m,n)
                except ValueError:
                    print("Os argumentos precisam ser inteiros")
            else:
                print("Essa funcionalidade exige dois argumentos e ambos inteiros")
        elif(argumentos[0] == "C"):
            if (len(argumentos) == 1):
                matriz.limpar()
            else:
                print("Essa funcionalidade não exige argumentos")
        elif(argumentos[0] == "L"):
            if (len(argumentos) == 4) and (not argumentos[3].isdigit()):
                try:
                    m = int (argumentos[1])
                    n = int( argumentos[2])
                    matriz.colorir_pixel(m,n, argumentos[3])
                except ValueError:
                    print("Os argumentos precisam ser inteiros")
            else:
                print("Essa funcionalidade exige três argumentos os dois primeiros inteiros e o último string")
        elif(argumentos[0] == "V"):
            if (len(argumentos) == 5) and (not argumentos[4].isdigit()):
                try:
                    m = int (argumentos[3])
                    n = int( argumentos[2])
                    coluna = int(argumentos[1])
                    matriz.colorir_vertical_pixel(coluna,m,n, argumentos[4])
                except ValueError:
                    print("Os argumentos precisam ser inteiros")
            else:
                print("Essa funcionalidade exige quatro argumentos os três primeiros inteiros e o último string")
        elif(argumentos[0] == "H"):
            if (len(argumentos) == 5) and (not argumentos[4].isdigit()):
                try:
                    m = int (argumentos[1])
                    n = int( argumentos[2])
                    linha = int(argumentos[3])
                    matriz.colorir_horizontal_pixel(linha,m,n, argumentos[4])
                except ValueError:
                    print("Os argumentos precisam ser inteiros")
            else:
                print("Essa funcionalidade exige quatro argumentos os três primeiros inteiros e o último string")
        elif(argumentos[0] == "K"):
            if (len(argumentos) == 6) and (not argumentos[5].isdigit()):
                try:
                    x1 = int (argumentos[1])
                    y1 = int( argumentos[2])
                    x2 = int(argumentos[3])
                    y2 = int(argumentos[4])
                    matriz.colorir_retangulo_pixel(x1,y1,x2,y2, argumentos[5])
                except ValueError:
                    print("Os argumentos precisam ser inteiros")
            else:
                print("Essa funcionalidade exige cinco argumentos os quatro primeiros inteiros e o último string")
        elif(argumentos[0] == "F"):
            if (len(argumentos) == 4) and (not argumentos[3].isdigit()):
                try:
                    m = int (argumentos[1])
                    n = int( argumentos[2])
                    matriz.colorir_regiao_pixel(m,n, argumentos[3])
                except ValueError:
                    print("Os argumentos precisam ser inteiros")
            else:
                print("Essa funcionalidade exige três argumentos os dois primeiros inteiros e o último string")
        elif(argumentos[0] == "S"):
            if (len(argumentos) == 2):
                arq = open(argumentos[1], 'w')
                arq.writelines(matriz.transformar_string())
                arq.close()
            else:
                print("Essa função exige dois argumentos")
        elif (argumentos[0] == "X"):
            sys.exit()
        else:
            pass

if __name__ == '__main__':
    main()